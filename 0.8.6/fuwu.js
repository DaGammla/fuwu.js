/*
* FUWU.js JavaScript Library v0.8.6
* https://gitlab.com/DaGammla/fuwu.js
*
* Released under the MIT license
* https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE
*
* 2022 by DaGammla
*/

function $o(cssSelector){
    return document.querySelector(cssSelector);
}

function $a(cssSelector, eachCallback){
    //Every element that matches the css selector
    let matches = document.querySelectorAll(cssSelector);

    if (eachCallback)
        matches.forEach(eachCallback);

    //Always return matches no matter of callback
    return matches;
}

const $ext = new Proxy({
    $lastVal: undefined, //Can be used by extensions for NodeList to see what the previous element returned
    $isList: undefined //Can be used by extensions to see whether they operate on a NodeList
}, {
    set(target, key, val){
        //Extensions for Nodes, NodeLists and Documents
        Node.prototype[key] = NodeList.prototype[key] = Document.prototype[key] = function(...args){
            //Save them for nested extensions
            let saveLast = $ext.$lastVal, saveList = $ext.$isList

            target.$lastVal = undefined
            target.$isList = this instanceof NodeList

            //Apply the extension
            let ret;
            if (target.$isList){
                for (let elem of this) {
                    //Apply for each element in the list
                    let v = val.apply(elem, args)
                    //Only overwrite the return value when given
                    if (v !== undefined){
                        ret = v
                    }
                    
                    target.$lastVal = v;
                }
            } else if (this instanceof Document) {
                ret = val.apply(this.documentElement, args)
            } else {
                ret = val.apply(this, args)
            }

            //Restore
            target.$lastVal = saveLast;
            target.$isList = saveList;

            if (ret === $ext)
                return this
            return ret
        }
        return true
    }
})


//Wrapped around in anonymous function to prevent global scope for readyCallbacks and domReady
//those shouldn't be possible to tamper with
const $domReady = function(){

    function listen(func){
        document.addEventListener("DOMContentLoaded", func);
    }

    let domReady = false;

    //Check if the dom is already ready
    if (document.readyState == "interactive" || document.readyState == "complete"){
        domReady = true;
    } else {
        //if not add a listener to it
        listen(()=>{
            domReady = true;
        });
    }

    //This is the actual $domReady function
    return function(callback){
        //If the dom is already ready
        if (domReady){
            //then execute the callback now
            callback();
        } else {
            //Otherweise add it to the list of callbacks that are waiting
            listen(callback);
        }
    }
}()

//$cookies object simplifies storing cookies
const $cookies = {

    //Set a cookie to a value. An expiry Date can be provided
    set(cookieName, cookieValue, expDate){

        //The path of this page
        let path = window.location.pathname;
        //The path of the folder, so the path until the last '/'
        path = path.substring(0, path.lastIndexOf("/") + 1);

        //Redirected to global cookies with the path of the local folder
        $cookies.g.set(cookieName, cookieValue, path, expDate);
    },

    //Returns the value of a cookie
    get(cookieName) {
        return $cookies.all()[cookieName];
    },
    
    //Clears a cookie
    clear(cookieName){
        //Remove the cookies value and set it to expire immediately
        $cookies.set(cookieName, "", new Date());
    },

    //Returns all cookies available to this page as an object
    all(){
        //All cookies available for this page
        let cookieString = document.cookie;

        //Every cookie is seperated by a ';'
        let cookieArray = cookieString.split(";");

        //Merge all cookies into an object map
        let cookieObject = cookieArray.reduce((allCookies, cookie)=>{

            let split = cookie.trim().split("=", 1); //Cookies look like: <name>=<value>;

            let cookieName = decodeURIComponent(split[0]);
            let cookieValue = decodeURIComponent(split[1]);

            return {...allCookies, [cookieName]: cookieValue}
        }, {});

        return cookieObject;
    },

    clearAll(){
        for (const key in this.all()) this.clear(key)
    },

    //T object for temporary cookies, that are deleted when the browser closes
    t: {

        //Set a cookie to a value
        set(cookieName, cookieValue){
            $cookies.set(cookieName, cookieValue, -1);
        }
    },

    //G object is for global cookies or cookies of a specific path
    g: {

        //Set a cookie to a value. A path and an expiry Date can be provided
        //Default path to "/" and expiry date to today in 8 years
        set(cookieName, cookieValue = "", path = "/", date = new Date(Date.now() + 8 * 365.25 * 24 * 60 * 60 * 1000)){

            let expires = ""
            if (date.toUTCString){
            //Partial string that sets the expiry date
                expires = ";expires="+ date.toUTCString();
            }

            //Partial string that sets the path
            let pathString = ";path=" + path;
            
            //encoding the cookie name and value to prevent problems with '=' or ';'
            document.cookie = 
                //cookieName=cookievalue
                encodeURIComponent(cookieName) + "="
                //if cookievalue doesn't exist, put ""
                + encodeURIComponent(cookieValue)
                //Append expiry and path
                + expires + pathString;
        },

        //Clears a global cookie or a cookie of a specific path
        clear(cookieName, path){
            //Remove the cookies value and set it to expire immediately
            $cookies.set(cookieName, "", path, new Date());
        },

        //G.T object is for temporary cookies with global or specific path
        t: {
            //Set a cookie to a value. A path can be provided
            set(cookieName, cookieValue, path){
                $cookies.g.set(cookieName, cookieValue, path, -1);
            }
        }
    },
    init(){
        
        //Redirect get, clear to $cookies
        let self = this;
        self.t.get = self.get;
        self.t.clear = self.clear;

        //Redirect get to $cookies
        self.g.get = self.get;

        //Redirect get, clear to $cookies
        self.g.t.get = self.get;
        self.g.t.clear = self.g.clear;

        delete self.init;
        return self;
    }
}.init();

//$storage is a wrapper for window.localStorage and window.sessionStorage
const $storage = {

    set: (k, v) => localStorage.setItem(k, v),

    get: (k) => localStorage.getItem(k),
    
    clear: (k) => localStorage.removeItem(k),

    //Returns all storage available to this page as an object
    all(){
        return JSON.parse(JSON.stringify(localStorage));
    },

    clearAll(){
        for (let key in this.all()) this.clear(key);
    },

    //T object for temporary cookies, that are deleted when the browser closes
    t: {
        set: (k, v) => sessionStorage.setItem(k, v),

        get: (k) => sessionStorage.getItem(k),
        
        clear: (k) => sessionStorage.removeItem(k),
    
        //Returns all temporary storage available to this page as an object
        all(){
            return JSON.parse(JSON.stringify(sessionStorage));
        },

        clearAll(){
            for (let key in this.all()) this.clear(key);
        },
    }
};

//$params object simplifies retrieving parameters from the url
const $params = {
    //Get the value of a parameter
    get(name, url){
        //With an URL instance, the parameters can easily be extracted from the url with .search
        let urlInstance = new URL(url != null ? url : window.location);
        //URLSearchParams can automatically find all parameters and their values
        let paramsInstance = new URLSearchParams(urlInstance.search);

        return paramsInstance.get(name);
    },
    //Get an array of all values of parameters with that name
    getEvery(name, url, eachCallback){

        //Url aargument is optional but in the middle of the arguments
        if (typeof url == 'function') {
            //So if theres a function on the second place, it is the callback
            eachCallback = arguments[1];
            url = null;
        }

        //With an URL instance, the parameters can easily be extracted from the url with .search
        let urlInstance = new URL(url == null ? url : window.location);
        //URLSearchParams can automatically find all parameters and their values
        let paramsInstance = new URLSearchParams(urlInstance.search);

        //All parameters with that name parsed by the URLSearchParams object
        let all = paramsInstance.getAll(name);

        //If there is a callback, call if for every element
        if (eachCallback){
            all.forEach(eachCallback);
        }

        //Even if there is a callback, also return the found parameter values
        return all;
    },
    has(name, url){
        //With an URL instance, the parameters can easily be extracted from the url with .search
        let urlInstance = new URL(url == null ? url : window.location);
        //URLSearchParams can automatically find all parameters and their values
        let paramsInstance = new URLSearchParams(urlInstance.search);

        return paramsInstance.has(name);
    },
    all(url){
        //With an URL instance, the parameters can easily be extracted from the url with .search
        let urlInstance = new URL(url == null ? url : window.location);
        //URLSearchParams can automatically find all parameters and their values
        let paramsInstance = new URLSearchParams(urlInstance.search);

        //Add all key value pairs to this object
        let paramsObject = {}
        for (const [key, value] of paramsInstance) {
            //Only use first of that key
            if (paramsObject[key]){
                continue;
            }
            paramsObject[key] = value;
        }
        return paramsObject;
    }
}

//$http object for http requests
const $http = {

    request(options, callback = _=>_, errorCallback = _=>_, synchronous){

        //Using js native XMLHttpRequest

        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onload = function() {
            //Check if the http request responded 200
            //200 being the response for OK, meaning the request was successful
            if (xmlHttp.status >= 200 && xmlHttp.status <= 299){
                //Call the callback when available
                callback(xmlHttp.responseText, xmlHttp.status, xmlHttp);
            } else {
                //If response code is different to 200 something did not go as expected
                //so call the errorCallback, when available
                errorCallback.call(xmlHttp, xmlHttp)
            }
        }

        xmlHttp.onerror = errorCallback;
        
        //Open the HttpRequest with all the parameters
        xmlHttp.open(options.method, options.url, !synchronous, options.user, options.password);

        if (options.headers){

            //Add all headers from the options.headers object, if any specified
            for (const key in options.headers) {
                xmlHttp.setRequestHeader(key, options.headers[key]);
            }

            //Header objects look like:{ "SOME_HEADER": "SOME_VALUE", "OTHER_HEADER": "OTHER_VALUE" }
        }

        //Send the HttpRequest
        xmlHttp.send(options.data);

        if (synchronous){
            //With synchronous requests the response is available here
            return xmlHttp.responseText
        }
    },

    
    //Redirect all http request methods to $http.request with their respective string
    //not using ...arguments as this is shorter in minified js

    get(url, callback, errorCallback){ $http.request({method:"GET", url:url}, callback, errorCallback) },
    post(url, data, callback, errorCallback){ $http.request({method:"POST", url:url, data:data}, callback, errorCallback) },

    //$http.s for synchronous http requests
    //shouldn't really be used on websites but can be really useful
    //when operation order is important
    s : {

        //Redirect $http.s.request to $http.request with synchronous set to true
        request(options, callback, errorCallback){ return $http.request(options, callback, errorCallback, true) },

        //Redirect all http request methods to $http.s.request with their respective string
        //not using ...arguments as this is shorter in minified js

        get(url, callback, errorCallback){ return $http.s.request({method:"GET", url:url}, callback, errorCallback) },
        post(url, data, callback, errorCallback){ return $http.s.request({method:"POST", url:url, data:data}, callback, errorCallback) },
    }
}

//$json object for parsing and stringfying json and for retrieving json objects from urls
const $json = {
    
    //Redirect $json to JSON for parse and string
    parse: JSON.parse,
    string: JSON.stringify,

    //Returns an js object represented by the json at the given url
    get(url, callback = _=>_, errorCallback){

        //Use http get request to perform this
        $http.get(url, function(responseText, statusCode, xmlHttp){
            //When a callback is given, call it with the parsed response form the http request
            callback($json.parse(responseText), statusCode, xmlHttp);
        }, errorCallback);
    },

    //$json.s for synchronous getting a json object
    s: {
        get(url, callback = _=>_, errorCallback){

            //Use http get request to perform this
            let responseText = $http.s.get(url, function(responseText, statusCode, xmlHttp){
                //When a callback is given, call it with the parsed response form the http request
                callback($json.parse(responseText), statusCode, xmlHttp);
            }, errorCallback);

            //Synchronous http requests don't necessarily need callback
            //the json object is also returned by this function

            return $json.parse(responseText);
        }
    }
}