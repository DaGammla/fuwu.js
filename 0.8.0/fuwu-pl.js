/*
* FUWU.js JavaScript Library v0.8.0
* https://gitlab.com/DaGammla/fuwu.js
*
* Released under the MIT license
* https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE
*
* 2022 by DaGammla
*/

const $fuwuProxy = (()=>{
    function attrProx(obj, prop, rec, fuwuProx){
        let val = Reflect.get(obj, prop, rec);
        if (typeof val == "object"){
            const handler = {
                set(obj, prop, newval, rec){
                    let old = Reflect.get(obj, prop, rec);
                    if (Reflect.set(obj, prop, newval, rec)){
                        if (old !== newval){
                            fuwuProx.triggerChange();
                        }
                        return true;
                    }
                    return false;
                },
                get(obj, prop, rec){
                    return attrProx(obj, prop, rec, fuwuProx);
                }
            };
            return new Proxy(val, handler);
        }
        return val;
    }

    return function(){
        let changeScheduled = false;
        const target = {
            $changeListeners:[],
            triggerChange(){
                if (!changeScheduled){
                    changeScheduled = true;
                    window.setTimeout(() => {
                        target.$changeListeners.forEach((l)=>{l.call(proxy, proxy)})
                        changeScheduled = false;
                    }, 1);
                }
            }
        };

        let proxy;

        const handler = {
            set(obj, prop, newval, rec){
                let old = Reflect.get(obj, prop, rec);
                if (Reflect.set(obj, prop, newval, rec)){
                    if (old !== newval){
                        target.triggerChange();
                    }
                    return true;
                }
                return false;
            },
            get(obj, prop, rec){
                return attrProx(obj, prop, rec, target);
            }
        };

        proxy = new Proxy(target, handler);
        return proxy;
    }
})();

(()=>{
    function makeScope(scope){
        let scopeAdd = ""
        for (const key of Object.keys(scope)) {
            scopeAdd += `const ${key}=${JSON.stringify(scope[key])};`
        }
        return scopeAdd;
    }

    function calcFplStep(el, parent, scope){
        switch (el.nodeName){
            case "F-IF":{

                let check = el.getAttribute("check");
                let result = false;
                if (check != null){
                    result = !!Function(makeScope(scope) + `return ${check};`).call(window);
                } else {
                    throw "F-IF without 'check' attribute";
                }

                if (result){
                    el.childNodes.forEach((nd)=>{
                        calcFplStep(nd, parent, scope);
                    });
                }

                if (el.nextElementSibling.nodeName == "F-ELSE"){
                    el.nextElementSibling.$prevCheck = result;
                }
                break;
            }
            case "F-ELSE":{
                if (el.$prevCheck == false){
                    el.childNodes.forEach((nd)=>{
                        calcFplStep(nd, parent, scope);
                    });
                } else if (el.$prevCheck == undefined){
                    throw "F-ELSE without F-IF";
                }
                break;
            }
            case "F-FOR":{
                let from = el.getAttribute("from");
                let arr = [];
                if (from != null){
                    arr = Function(makeScope(scope) + `return ${from};`).call(window);
                } else {
                    throw "F-FOR without 'from' attribute";
                }

                let name = el.getAttribute("as");
                if (name == null){
                    name = "$data";
                }

                for (const ele of arr) {
                    let newScope = { ...scope };
                    newScope[name] = ele;
                    el.childNodes.forEach((nd)=>{
                        calcFplStep(nd, parent, newScope);
                    });
                }
                break;
            }
            case "#text":{
                let clone = el.cloneNode(false);
                clone.textContent = el.textContent.replaceAll(/{{([^}]|}[^}])*}}/g, (match) => {
                    let val = match.substr(2, match.length - 4);
                    return Function(makeScope(scope) + `return ${val};`).call(window);
                });
                parent.appendChild(clone);
                break;
            }
            default:{
                let clone = el.cloneNode(false);
                if (clone.setAttribute){
                    let fAttr = clone.getAttribute("f-attr");
                    if (fAttr != null){
                        let attrs = Function(makeScope(scope) + `return {${fAttr}};`).call(window);
                        for (const key of Object.keys(attrs)) {
                            clone.setAttribute(key, attrs[key]);
                        }
                        clone.removeAttribute("f-attr");
                    }
                }
                el.childNodes.forEach((nd) => {
                    calcFplStep(nd, clone, scope);
                })
                parent.appendChild(clone);
            }
        }
    }

    function recalcFpl(el){
        let done = document.createElement("f-dn");
        el.$originalFpl.childNodes.forEach((nd)=>{
            calcFplStep(nd, done, {});
        })
        if (el.$lastFdn != null){
            el.$lastFdn.forEach(nd => nd.remove());
        }
        el.$lastFdn = [...done.childNodes];
        for (let i = done.childNodes.length - 1; i >= 0; i--){
            el.parentNode.insertBefore(done.childNodes[i], el.nextSibling);
        }
    }

    const appliedListeners = [];
    function applyFpl(){
        document.querySelectorAll("f-pl:not([f-applied])").forEach((el) => {
            if (!appliedListeners.includes(el)){
                appliedListeners.push(el);
                if (el.hasAttribute("data")){
                    let proxies = el.getAttribute("data");
                    proxies = Function(`return [${proxies}];`).call(window);
                    proxies.forEach((prox) => {
                        prox.$changeListeners.push(() => {
                            recalcFpl(el);
                        })
                    })
                }
                el.setAttribute("f-applied", "");
                el.$originalFpl = el.cloneNode(true)
                el.innerHTML = "";
                recalcFpl(el);
            }
        })
    }

    const observer = new MutationObserver(function(mutationList, ob){
        out: for (const mutation of mutationList) {
            if (mutation.type === 'childList') {
                if (mutation.addedNodes){
                    for (const node of mutation.addedNodes) {
                        if (node.nodeName == "F-PL"){
                            applyFpl();
                            return;
                        }
                    }
                }
            }
        }
    });

    applyFpl();

    observer.observe(document.documentElement, { childList: true, subtree: true });

    const css = "<style>f-pl{ display: none }</style>";

    document.head.insertAdjacentHTML("beforeend", css);
})()