/*
* FUWU.js JavaScript Library vX.Y.Z
* https://gitlab.com/DaGammla/fuwu.js
*
* Released under the MIT license
* https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE
*
* JJJJ by DaGammla
*/

$ext.hide = function(){
    this.style.display = "none"
    return $ext
}

$ext.show = function(){
    this.style.display = ""
    return $ext
}

$ext.addClass = function(name){
    this.classList.add(name)
    return $ext
}

$ext.after = function(elem){
    this.insertAdjacentElement('afterend', elem)
    return $ext
}

$ext.before = function(elem){
    this.insertAdjacentElement('beforebegin', elem)
    return $ext
}

$ext.append = function(elem){
    this.appendChild(elem)
    return $ext
}

$ext.clone = function(){
    let clone = this.cloneNode(true)
    if ($ext.$isList){
        if ($ext.$lastVal === undefined){
            return [clone].asNodeList();
        }
        return Array.from($ext.$lastVal).concat([clone]).asNodeList()
    }
    return clone
}

$ext.containsQuery = function(select){
    return $ext.$lastVal || !!this.querySelector(select)
}

$ext.each = function(fun){
    fun(this)
    return $ext
}

$ext.empty = function(){
    while(this.firstChild)
        this.removeChild(this.firstChild);

    return $ext
}

Array.prototype.asNodeList = function() {

    if (this.length <= 0){
        return document.createDocumentFragment().childNodes
    }

    function buildIndexCSSPath(elem) {
        if (elem == elem.ownerDocument.documentElement){
            return ":root"
        }
        let parent = elem.parentElement;

        return buildIndexCSSPath(parent) + " > :nth-child(" + (Array.from(parent.children).indexOf(elem) + 1) + ")";
    }

    let dom = document
    let notInDocument = this.filter((elem) => {
        if (elem.ownerDocument != null)
            dom = elem.ownerDocument;
        return elem.ownerDocument == null || !elem.ownerDocument.documentElement.contains(elem);
    })
    notInDocument.forEach(elem => dom.documentElement.appendChild(elem))

    let names = this.map(elem => buildIndexCSSPath(elem));
    let superSelector = names.join(",");

    let res = dom.querySelectorAll(superSelector);
    notInDocument.forEach(elem => elem.remove())
    return res;
}

$ext.findAll = function(select){
    return this.querySelectorAll(select)
}

$ext.find = function(select){
    return this.querySelector(select)
}

$ext.attr = function(name, val){
    if (val === undefined){
        return this.getAttribute(name)
    } else {
        this.setAttribute(name, val)
        return this
    }
}

$ext.height = function(val){
    if (val === undefined)
        return parseFloat(getComputedStyle(this, null).height.replace("px", ""))

    if (typeof val === "function") val = val();
    if (typeof val === "string") this.style.height = val;
    else this.style.height = val + "px";
    return $ext
}

$ext.width = function(val){
    if (val === undefined)
        return parseFloat(getComputedStyle(this, null).width.replace("px", ""))

    if (typeof val === "function") val = val();
    if (typeof val === "string") this.style.width = val;
    else this.style.width = val + "px";
    return $ext
}

$ext.html = function(val){
    if (val === undefined)
        return this.innerHTML
    this.innerHTML = val
    return $ext
}

$ext.text = function(val){
    if (val === undefined)
        return this.textContent
    this.textContent = val
    return $ext
}

$ext.css = function(rule, val){
    if (val === undefined)
        return getComputedStyle(this)[rule];
    
    this.style[rule] = val
    return $ext
}

$ext.hasClass = function(cla){
    return ($ext.$lastVal === undefined ? true : $ext.$lastVal) && this.classList.contains(cla);
}

$ext.index = function(){
    let el = this
    let i = 0;
    while (el = el.previousElementSibling) {
        i++;
    }
    return i;
}

$ext.is = function(select){
    return ($ext.$lastVal === undefined ? true : $ext.$lastVal) && (this === select || this.matches(select))
}

$ext.next = function(){
    return this.nextElementSibling
}

$ext.offset = function(){
    let rect = this.getBoundingClientRect();
    return {
      top: rect.top + document.body.scrollTop,
      left: rect.left + document.body.scrollLeft
    }
}

$ext.outerHeight = function(marg){
    if (marg){
        let height = this.offsetHeight;
        let style = getComputedStyle(this);
        height += parseInt(style.marginTop) + parseInt(style.marginBottom);
        return height;
    } else {
        return this.offsetHeight
    }
}

$ext.outerWidth = function(marg){
    if (marg){
        let width = this.offsetWidth;
        let style = getComputedStyle(this);
        width += parseInt(style.marginLeft) + parseInt(style.marginRight);
        return width;
    } else {
        return this.offsetWidth
    }
}

$ext.parent = function(){
    return this.parentNode
}

$ext.position = function(){
    return {
        left: this.offsetLeft, top: this.offsetTop
    }    
}

$ext.prepend = function(elem){
    this.insertBefore(elem, this.firstChild);
    return $ext
}

$ext.prev = function(){
    return this.previousElementSibling
}

$ext.remove = function(){
    if (this.parentNode !== null) {
        this.parentNode.removeChild(this);
    }
    return $ext
}

$ext.removeAttr = function(name){
    this.removeAttribute(name)
    return $ext
}

$ext.removeClass = function(name){
    this.classList.remove(name);
    return $ext
}

$ext.replaceWith = function(str){
    if (typeof str !== "string"){
        str = str.outerHTML;
    }
    this.outerHTML = str
    return $ext
}

$ext.siblings = function(str){
    if (this.parentNode === null) return [].asNodeList();
    if (str === undefined){
        str = "*";
    }
    return Array.from(this.parentNode.children).filter(a => a !== this && a.is(str)).asNodeList();
}

$ext.toggleClass = function(name){
    this.classList.toggle(name)
    return $ext
}

$ext.click = function(lis){
    this.addEventListener("click", lis, false)
    return $ext
}

$ext.on = function(name, lis){
    this.addEventListener(name, lis)
    return $ext
}

$ext.off = function(name, lis){
    this.removeEventListener(name, lis)
    return $ext
}