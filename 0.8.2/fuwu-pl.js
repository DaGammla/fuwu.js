/*
* FUWU.js JavaScript Library v0.8.2
* https://gitlab.com/DaGammla/fuwu.js
*
* Released under the MIT license
* https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE
*
* 2022 by DaGammla
*/

const $fuwuProxy = (()=>{

    let proxiesMap = new Map();

    //The actual $fuwuProxy function
    //Creates a proxy that can be listened from by f-pl tags
    let fuwuProx = ()=>{
        //Don't retrigger changes when already scheduled
        let changeScheduled = false;

        let proxy;

        let fuwuProx = {
            $listeners:[],
            $triggerChange(){
                if (!changeScheduled){
                    changeScheduled = true;
                    //Only schedule once
                    setTimeout(() => {
                        //Call all listeners when the js engine is idling
                        for (let l of fuwuProx.$listeners) {
                            l.call(proxy, proxy);
                        }

                        changeScheduled = false;
                    });
                }
            },
            $addFpl(element, recalcNow){
                if (element.nodeName != "F-PL"){
                    throwError("You can only make F-PL elements listen");
                }
                //Push a recalculation to the listeners
                this.$listeners.push(() => recalcFpl(document.getElementById(element.id)));

                if (recalcNow){
                    recalcFpl(element);
                }
            }
        };

        let handler = makeHandler(fuwuProx);

        proxy = new Proxy(fuwuProx, handler);

        proxiesMap.set(fuwuProx, proxy);
        return proxy;
    }

    //Wraps in a Proxy to detect changes
    let attrProx = (obj, prop, rec, fuwuProx) => {

        //Always true, used to prevent double wrapping a proxy
        if (prop == "$isProxy") return true;
        //Allow access to the target item
        if (prop == "$target") return obj;

        let val = Reflect.get(obj, prop, rec);
        //Only needed for objects
        if (typeof val == "object"){

            //If there is no proxy already create one
            let proxy = proxiesMap.get(val) ?? new Proxy(val, makeHandler(fuwuProx));
            
            proxiesMap.set(val, proxy);

            return proxy;
        }
        return val;
    }

    let makeHandler = fuwuProx => {
        let handler = {
            set(obj, prop, newval, rec){
                //Dont set proxies but their content
                if (newval.$isProxy){
                    newval = newval.$target;
                }
                //old used to check if it actually changes
                let old = Reflect.get(obj, prop, rec);
                if (Reflect.set(obj, prop, newval, rec)){
                    if (old !== newval){ //Save performance
                        fuwuProx.$triggerChange();
                    }
                    return true;
                }
                return false;
            },
            get(obj, prop, rec){
                //Wrap childs again for change detection all the way down
                return attrProx(obj, prop, rec, fuwuProx);
            }
        };
        return handler;
    }

    /**
     * Lots of small functionalities are extracted into an seperate 
     * This is because minifies and compilers then only change code once
     */

    //Dict that contains all set listeners
    let appliedListeners = {};

    //Dict that contains maps which map template sources to F-IN elements
    let inTemplates = {}

    //Clone the scope for children
    let cloneScope = scope => ({...scope});
    
    //Create an a new array from a source
    let cloneToArray = Array.from;

    //Returns the keys of an object as an array
    let keysOfObject = Object.keys;

    //Run a fun for each item of a iterable
    let each = (iterable, fun) => {
        let i = 0;
        for (let el of iterable)
            fun(el, i++);
    }

    //Run code with a scope
    let runCode = (str, scope) => {
        let val = Function(`const { ${keysOfObject(scope).join()} } = arguments[0]; ${str};`).call(window, scope);
        return val;
    }

    //Evaluate an expression with a scope
    let evaluateExpression = (str, scope) => runCode("return " + str, scope);
    
    //Get a new node or reuse an old one
    let getUseNode = (sourceEl, forItem, forIndex, oldMapBuilder)=>{
        let oldMap, arr, returnEl;
        //If old map is null or the array in the map is null or no array element at that index
        if (!(oldMap = sourceEl.$oldMap) || !(arr = oldMap.get(forItem)) || !(returnEl = arr[forIndex])){
            returnEl = sourceEl.cloneNode(); //Create a new node
        }
        //Empty the element if it has inner code
        returnEl.textContent = "";

        //Add this object to the array to later create the new $oldMap afterwards
        oldMapBuilder.push({sl: sourceEl, el: returnEl, it: forItem, id: forIndex});
        return returnEl;
    }
    
    //A method to throw an error. Can be used inline for nullish ??
    let throwError = (err) => { throw err; }

    //Get an attribute from an element or return an default value
    let attr = (el, attribute, def) => el.getAttribute(attribute) ?? def;

    //Get an attribute from an element or throw an error if nullish
    let attrOrError = (el, attribute, error) => attr(el, attribute) ?? throwError(error);

    //Get a element source from an id
    let getSource = id => {
        let source = appliedListeners[id];
        //If there is no applied listener search for the id in the document
        if (source == null){
            let element = document.getElementById(id);

            //Will be null when the element was not found
            let nodeName = element?.nodeName;

            //Templates content property returns a document fragment
            if (nodeName === "TEMPLATE"){
                element = element?.content;
            }

            //Don't do f-pls cause they should be in the appliedListeners 
            if (nodeName !== "F-PL"){
                source = element;
            }
        }
        return source;
    }

    let steps = {
        ["F-IF"](el, scope, ...otherArgs){
            //The test condition
            let check = attrOrError(el, "check", "F-IF without 'check' attribute");
            //Evaluate the test
            let result = !!evaluateExpression(check, scope);

            if (result){
                let newScope = cloneScope(scope); //Children get their own shared scope
                //Only calculate/add children if true
                calcAll(el, newScope, ...otherArgs);
            }

            //If the next element is an else provide it with the ifs result
            let ns = el.nextElementSibling;
            if (ns?.nodeName == "F-ELSE"){
                ns.$prvChck = result;
            }
        },

        ["F-ELSE"](el, scope, ...otherArgs){
            //$prevCheck is nullish if no F-IF has set it before
            let prevCheck = el.$prvChck ?? throwError("F-ELSE without F-IF");
            //Add children if the if before was false
            if (!prevCheck){
                let newScope = cloneScope(scope); //Children get their own shared scope
                calcAll(el, newScope, ...otherArgs);
            }
        },

        ["F-FOR"](el, scope, parent, id, _, __, oldMapBuilder){
            let from = attrOrError(el, "from", "F-FOR without 'from' attribute");

            //Evaluate over which array to operate
            let fromValue = evaluateExpression(from, scope)
            let arr = cloneToArray(fromValue);

            //How to call the item in the child scope
            let as = attr(el, "as", "$item");

            //How to call the index in the child scope
            let index = attr(el, "index", "$index");

            each(arr, (ele, i)=>{
                //New scope for the children with item and index
                let newScope = { ...scope, [index] : i, [as] : ele };
                
                //Represents how many occurances of this array element came before
                let keepId = arr.filter((it, index) => index < i && it === ele).length;

                //For each item in the array then calculate/add each of the children
                calcAll(el, newScope, parent, id, ele, keepId, oldMapBuilder);
            });
        },

        ["F-VAR"](el, scope){
            let name = attr(el, "name"),
                value = attr(el, "value"),
                def = attr(el, "def");

            //If def is given add all of its entries as scope items
            if (def != null){
                //Evaluate def objec
                let vars = evaluateExpression(`{${def}}`, scope);
                //Get all entries from the def object
                let varEntry = Object.entries(vars);
                //Add all entries to the scope
                each(varEntry, ([key, value]) => scope[key] = value);
                return;
            }

            if (!name == null || value == null)
                throwError("F-VAR needs a 'name' and 'value' or a 'def' attribute");

            //Add this variable to the scope
            scope[name] = evaluateExpression(value, scope);
        },

        ["F-IN"](el, scope, parent, id, ...otherArgs){
            //The id of which template to use
            let inId = attrOrError(el, "in-id", "F-IN without 'in-id' attribute");
                
            //Recursion would break lots of stuff
            if (id.includes(inId)) throwError("Recursive templating is not allowed");

            //Get the templateSources for this F-PL or create a new map
            let inTmp = inTemplates[id[0]] ?? (inTemplates[id[0]] = new Map());
            //Get the specific source for this F-IN or try to set it
            let source = inTmp.get(el);

            if (source == null){
                //If there is no source try finding it by id
                source = getSource(inId)?.cloneNode(true);
                //Then save it in the map
                inTmp.set(el, source);
            }
            
            if (source != null){
                //A source has been found check if it is up to date
                let currentInner = getSource(inId)?.innerHTML ?? source.innerHTML;
                //Check if the currently found source has the same content
                if (source.innerHTML != currentInner){
                    source.innerHTML = currentInner; //If not copy it
                }

                let newScope = cloneScope(scope); //Children get their own shared scope
                //Add/calculate all children
                calcAll(source, newScope, parent, [...id, inId], ...otherArgs);
            }
        },

        ["#default"](el, scope, parent, id, forItem, forIndex, oldMapBuilder){
            //All other nodes get copied
            let clone = getUseNode(el, forItem, forIndex, oldMapBuilder);

            let attributes = el.attributes;
            if (attributes){
                //Replace all 'f:...' attributes like this:
                //<tag f:[attr]="[code]" /> => <tag [attr]="[evaluated]" />
                let fixedAttribute = cloneToArray(attributes);
                each(fixedAttribute, attr => {
                    let [ prefix, ...rest ] = attr.name.split(":");
                    let name = rest.join(":"); //Rejoin the leftover splits
                    if (name?.length){
                        if (prefix == "f") {
                            //Set evaluted attribute
                            clone.setAttribute(name, evaluateExpression(attr.value, scope));
                            //Remove f:... attribute
                            clone.removeAttribute(attr.name);

                        } else if (prefix == "evt"){
                            //If it starts with evt:... then carry it over for the next iteration
                            clone.setAttribute(attr.name, attr.value);
                        }
                    }
                });

                let fixedCloneAttribute = cloneToArray(clone.attributes);

                //Add all 'evt:<event name>' attributes as listeners
                each(fixedCloneAttribute, attr => {
                    let [ prefix, ...rest ] = attr.name.split(":");
                    let name = rest.join(":"); //Rejoin the leftover 
                    
                    
                    if (name?.length && prefix == "evt") {
                        //The property name under which the last listener will be saved
                        let lastName = "$last-" + attr.name;

                        let lastListener = clone[lastName];
                        if (lastListener){ //If there is a listener, remove it
                            clone.removeEventListener(name, lastListener);
                        }

                        //The method that the event will call
                        let listener = (event) => {
                            //Add the event to the call scope
                            let listenerScope = {...scope, $event: event};

                            //Evaluate
                            runCode(attr.value, listenerScope);
                        };

                        clone.addEventListener(name, listener);

                        //Save the last listener, so it can be removed next time
                        clone[lastName] = listener;

                        //Remove evt:... attribute
                        clone.removeAttribute(attr.name);
                    }
                });

            } else { //If there are not attributes it's a text node

                //Find all places in text nodes that look like this: {{<code>}}
                clone.textContent = el.textContent.replaceAll(/{{[^]+?}}/g, match => {
                    //Eval all these matches and replace
                    let val = match.slice(2, match.length - 2);
                    return evaluateExpression(val, scope);
                });
            }
            let newScope = cloneScope(scope); //Children get their own shared scope
            //Calculate/add all children
            calcAll(el, newScope, clone, id, forItem, forIndex, oldMapBuilder);
            //Add the copy
            parent.appendChild(clone);
        }
    }

    //Calculate the fpl behavior for child nodes
    let calcAll = (el, scope, parent, id, forItem, forIndex, oldMapBuilder) => {
        //Add/calculate all children
        each(el.childNodes, (nd) => {
            let step = steps[nd.nodeName] ?? steps["#default"];
            step(nd, scope, parent, id, forItem, forIndex, oldMapBuilder);
        });
    }

    //Recursively get all childNodes into an array
    let clearOldMap = el => {
        if (el){
            each(el.childNodes, (nd)=>{
                nd.$oldMap = new Map();
                clearOldMap(nd);
            })
        }
    }

    //Recalculates a f-pl
    let recalcFpl = el => {

        //Get id and corresponding source
        let source = appliedListeners[el.id];
        if (source == null) return;

        //All created elements will be stored
        let done = document.createElement("f-d");

        let oldMapBuilder = [];
        let oldClones = [];
        let ids = [el.id];

        //If there are old elements temporarily clone them as a visual placeholder while calculating
        source.$lastFdn?.forEach(nd=>{
            let clone = nd.cloneNode(true);
            oldClones.push(clone); //So they can be removed later
            nd.parentNode?.replaceChild(clone, nd); //replaceChild(new, old)
        });


        //Add/calculate all child nodes to be added to the done element

        //Source Element, Empty Scope, Target Element, Id already in the restricted array
        //null is the forItem, 0 is the for id, Array to build the old maps
        calcAll(source, {}, done, ids, null, 0, oldMapBuilder);

        //Create an array from the child nodes and save them to be removed next time
        source.$lastFdn = cloneToArray(done.childNodes);

        //Insert all elements after this node
        //In reverse order so the last item in the dom was last in the list
        source.$lastFdn.reduceRight((_, child) => el.parentNode.insertBefore(child, el.nextSibling))
        
        //Remove all temporary clones
        each(oldClones, c => c.remove());

        //Clear the old map
        clearOldMap(source)

        //Also for all F-IN sources 
        inTemplates[el.id]?.forEach(clearOldMap);
        
        //Save all old elements to be reused on the next calculations
        each(oldMapBuilder, (build)=>{
            //SourceElement, Element, ForItem, ForId
            let {sl, el, it, id} = build;

            let obj = sl.$oldMap.get(it) ?? {} //Get the object of the specific ForItem
            obj[id] = el; //Set the old element at the right position
            sl.$oldMap.set(it, obj); //Reassign obj; Needed because of nullish
        });
    }

    //Get the next available id
    //Start with length + 1 but check if it is available
    let nextId = () => {
        let keys = keysOfObject(appliedListeners);
        let key = keys.length + 1;
        let keyFull
        while (keys.includes((keyFull = "f-pl-" + key))) key++;
        appliedListeners[keyFull] = null;
        return keyFull;
    }

    //Call when new f-pl tags are added to the dom
    let applyFpl = () => {
        //Create an id for all f-pl without one
        let fplWithoutId = document.querySelectorAll("f-pl:not([id])")
        each(fplWithoutId, e => e.id = nextId());

        //Now for all all f-pl with an id
        let fplWithId = document.querySelectorAll("f-pl[id]");
        each(fplWithId, (el) => {
            //If the id has not already applied
            if (!appliedListeners[el.id]){

                //Save the contents under its id, then clear it
                appliedListeners[el.id] = el.cloneNode(true);
                el.innerHTML = "";

                //Get which proxies this f-pl relies on
                let data = attr(el, "data", "");

                //Evalute the proxies
                let itemFromData = evaluateExpression(`[${data}]`, {})
                
                //Add this element to all proxies
                each(itemFromData, prox => prox.$addFpl(el));

                recalcFpl(el);
            }
        });
    }

    //Create an observer, that listens for f-pls being added
    let observer = new MutationObserver(mutationList=>{
        for (let mutation of mutationList) {
            for (let node of mutation.addedNodes ?? []) {
                //For all added nodes if it one is a f-pl apply it
                if (node.nodeName == "F-PL"){
                    applyFpl();
                    return;
                }
            }
        }
    });
    //Observe the document
    observer.observe(document.documentElement, { childList: true, subtree: true });

    //Apply all already existing f-pl just in case
    applyFpl();

    //Add css hiding f-pl after this script tag
    let css = "<style>f-pl,f-if,f-else,f-for,f-var,f-in{display:none}</style>";
    document.currentScript.insertAdjacentHTML("afterend", css);
    
    return fuwuProx;
})();