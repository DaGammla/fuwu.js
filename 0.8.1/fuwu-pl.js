/*
* FUWU.js JavaScript Library v0.8.1
* https://gitlab.com/DaGammla/fuwu.js
*
* Released under the MIT license
* https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE
*
* 2022 by DaGammla
*/

const $fuwuProxy = (()=>{

    //Wraps in a Proxy to detect changes
    function attrProx(obj, prop, rec, fuwuProx){

        //Always true, used to prevent double wrapping a proxy
        if (prop == "$isFuwuProxy") return true;

        let val = Reflect.get(obj, prop, rec);
        //Only needed for objects
        if (typeof val == "object"){
            //Don't wrap fuwu proxies again
            if (val.$isFuwuProxy) return val;
            let handler = {
                set(obj, prop, newval, rec){
                    //old used to check if it actually changes
                    let old = Reflect.get(obj, prop, rec);
                    if (Reflect.set(obj, prop, newval, rec)){
                        if (old !== newval){ //Save performance
                            fuwuProx.triggerChange();
                        }
                        return true;
                    }
                    return false;
                },
                get(obj, prop, rec){
                    //Wrap childs again for change detection all the way down
                    return attrProx(obj, prop, rec, fuwuProx);
                }
            };
            return new Proxy(val, handler);
        }
        return val;
    }

    //The actual $fuwuProxy function
    //Creates a proxy that can be listened from by f-pl tags
    return function(){
        //Don't retrigger changes when already scheduled
        let changeScheduled = false;

        let proxy;

        let target = {
            $changeListeners:[],
            triggerChange(){
                if (!changeScheduled){
                    changeScheduled = true;
                    //Only schedule once
                    window.setTimeout(() => {
                        //Call all listeners when the js engine is idling
                        for (let l of target.$changeListeners) {
                            l.call(proxy, proxy);
                        }
                        changeScheduled = false;
                    }, 1);
                }
            }
        };

        let handler = {
            set(obj, prop, newval, rec){
                //old used to check if it actually changes
                let old = Reflect.get(obj, prop, rec);
                if (Reflect.set(obj, prop, newval, rec)){
                    if (old !== newval){ //Save performance
                        target.triggerChange();
                    }
                    return true;
                }
                return false;
            },
            get(obj, prop, rec){
                //Wrap children in proxies to trigger changes
                return attrProx(obj, prop, rec, target);
            }
        };

        return proxy = new Proxy(target, handler);
    }
})();

var _$evlScp = undefined;

(()=>{
    //Dict that contains all set listeners
    let appliedListeners = {};

    //Add all variables from the scope in front of the executed code as constants
    function makeScope(scope){
        let keys = Object.keys(scope);
        if (keys.length <= 0) return "";
        let scopeAdd = "const {";
        for (let key of keys) {
            scopeAdd += key + ",";
        }
        return scopeAdd + "} = _$evlScp;";
    }

    //Evaluate an expression with a scope
    function cal(str, scope){
        _$evlScp = scope;
        let val = Function(`${makeScope(scope)}return ${str};`).call(window);
        _$evlScp = undefined;
        return val;
    }

    function calcAll(el, parent, scope, id){
        //Add/calculate children if the F-IF before was false
        for (let nd of el.childNodes){
            calcFplStep(nd, parent, scope, id);
        }
    }

    //Calculate the fpl behavior for a single element (el)
    //id: array of all referenced listeners. Needed to prevent recursion
    function calcFplStep(el, parent, scope, id){
        switch (el.nodeName){
            case "F-IF":{
                //The test condition
                let check = el.getAttribute("check");
                if (check == null)
                    throw "F-IF without 'check' attribute";
                //Evaluate the test
                let result = !!cal(check, scope);

                if (result){
                    scope = {...scope}; //Children get their own shared scope
                    //Only calculate/add children if true
                    calcAll(el, parent, scope, id);
                }

                //If the next element is an else provide it with the ifs result
                if (el.nextElementSibling != null && el.nextElementSibling.nodeName == "F-ELSE"){
                    el.nextElementSibling.$prevCheck = result;
                }
                break;
            }
            case "F-ELSE":{
                //Its undefined if no F-IF has set it before
                if (el.$prevCheck == undefined){
                    throw "F-ELSE without F-IF";
                }
                else if (el.$prevCheck == false){
                    scope = {...scope}; //Children get their own shared scope
                    calcAll(el, parent, scope, id);
                }
                break;
            }
            case "F-FOR":{
                let fr = el.getAttribute("from");
                if (fr == null)
                    throw "F-FOR without 'from' attribute";

                //Evaluate over which array to operate
                let arr = cal(fr, scope)

                //How to call the item in the child scope
                let as = el.getAttribute("as");
                if (as == null){
                    as = "$item";
                }

                //How to call the index in the child scope
                let index = el.getAttribute("index");
                if (index == null){
                    index = "$index";
                }

                let i = 0; //Count the index
                for (let ele of arr) {
                    //New scope for the children with item and index
                    let newScope = { ...scope, [index] : i, [as] : ele };
                    //For each item in the array then calculate/add each of the children
                    calcAll(el, parent, newScope, id);
                    i++;
                }
                break;
            }
            case "F-VAR":{
                let name = el.getAttribute("name"), value = el.getAttribute("value"), def = el.getAttribute("def");

                if (def != null){
                    def = cal(`{${def}}`, scope)
                    for (let [key, value] of Object.entries(def)) {
                        scope[key] = value;
                    }
                    break;
                }

                if (name == null || value == null) throw "F-VAR needs a 'name' and 'value' or a 'def' attribute";

                //Add this variable to the scope
                scope[name] = cal(value, scope);
                break;
            }
            case "F-IN":{
                //The id of which template to use
                let inId = el.getAttribute("f-id");
                if (inId == null) throw "F-IN without 'f-id' attribute";
                
                //Recursion would break lots of stuff
                if (id.includes(inId)) throw "Recursive listeners are not allowed";

                //Get the source of the children to be used
                let source = appliedListeners[inId];
                if (source != null){
                    scope = {...scope}; //Children get their own shared scope
                    //Add/calculate all children
                    calcAll(source, parent, scope, [...id, inId]);
                }

                break;
            }
            case "#text":{
                let clone = el.cloneNode(true);
                //Find all places in text nodes that look like this: {{<code>}}
                clone.textContent = el.textContent.replaceAll(/{{([^}]|}[^}])*}}/g, (match) => {
                    //Eval all these matches and replace
                    let val = match.substr(2, match.length - 4);
                    return cal(val, scope);
                });
                parent.appendChild(clone);
                break;
            }
            default:{
                //All other nodes get copied
                let clone = el.cloneNode(false);
                //With all 'f:...' attributes replaced like this:
                //<tag f:[attr]="[code]" /> => <tag [attr]="[evaluated]" />
                if (el.attributes){
                    for (let attr of el.attributes){
                        if (attr.name.startsWith("f:") && attr.name.length > 2){
                            //Set evaluted attribute
                            clone.setAttribute(attr.name.substr(2), cal(attr.value, scope));
                            //Remove f:... attribute
                            clone.removeAttribute(attr.name);
                        }
                    }
                }
                scope = {...scope}; //Children get their own shared scope
                //Calculate/add all children
                calcAll(el, clone, scope, id);
                //Add the copy
                parent.appendChild(clone);
            }
        }
    }

    //Recalculates a f-pl
    function recalcFpl(el){

        //Get id and corresponding source
        let id = el.getAttribute("f-id");
        let source = appliedListeners[id];
        if (source == null) return;

        //All created elements will be stored
        let done = document.createElement("f-dn");
        //Templates wont have new elements added
        if (!el.getAttribute("template")){
            //Add/calculate all child nodes to be added to the done element
            //Start with empty scope {} and the id already restricted for recursion [id]
            calcAll(source, done, {}, [id]);
        }
        //IF there are old elements remove them all
        if (source.$lastFdn != null){
            source.$lastFdn.forEach(nd => nd.remove());
        }

        //Create an array from the child nodes and save them to be removed next time
        source.$lastFdn = [...done.childNodes];

        //Insert all elements after this node
        //In reverse order so the last item in the dom was last in the list
        for (let i = source.$lastFdn.length - 1; i >= 0; i--){
            el.parentNode.insertBefore(done.childNodes[i], el.nextSibling);
        }
    }

    //Get the next available id
    //Start with length + 1 but check if it is available
    function nextId(){
        let keys = Object.keys(appliedListeners);
        let key = keys.length + 1;
        while (keys.includes("f-pl-" + key)) key++;
        return "f-pl-" + key;
    }

    //Call when new f-pl tags are added to the dom
    function applyFpl(){
        //Create an id for all f-pl without one and set is as an attribute
        for (let el of document.querySelectorAll("f-pl:not([f-id])")) {
            let id = nextId();
            el.setAttribute("f-id", id);
        }

        //Now for all all f-pl with an id
        for (let el of document.querySelectorAll("f-pl[f-id]")){
            let id = el.getAttribute("f-id");
            //If the id has not already applied
            if (!Object.keys(appliedListeners).includes(id)){

                //Save the contents under its id, then clear it
                appliedListeners[id] = el.cloneNode(true);
                el.innerHTML = "";

                //Get which proxies this f-pl relies on
                let data = el.getAttribute("data");
                if (data != null){
                    //Evalute the proxies
                    for(let prox of cal(`[${data}]`, [])){
                        //Add to the proxy a listener that recalculates the f-pl with this id
                        prox.$changeListeners.push(() => {
                            document.querySelectorAll(`f-pl[f-id=${JSON.stringify(id)}]`).forEach(recalcFpl);
                        })
                    }
                }
                //Calculate this f-pl now
                recalcFpl(el);
            }
        }
    }

    //Create an observer, that listens for f-pls being added
    let observer = new MutationObserver(function(mutationList, ob){
        for (let mutation of mutationList) {
            if (mutation.addedNodes){
                for (let node of mutation.addedNodes) {
                    //For all added nodes if it one is a f-pl apply it
                    if (node.nodeName == "F-PL"){
                        applyFpl();
                        return;
                    }
                }
            }
        }
    });
    //Observe the document
    observer.observe(document.documentElement, { childList: true, subtree: true });

    //Apply all already existing f-pl just in case
    applyFpl();

    //Add css hiding f-pl after this script tag
    let css = "<style>f-pl{ display: none }</style>";
    document.currentScript.insertAdjacentHTML("afterend", css);
})()