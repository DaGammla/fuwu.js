const uglifyJS = require("uglify-js");

function minify(code){
    let result = uglifyJS.minify(code, {compress:{ passes: 3 }});
    console.log(result.error); // runtime error, or `undefined` if no error
    return result.code
}

const readline = require('readline');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Enter the version number like this: 'X.Y.Z': ", (answer) => {
    versionNumberRead(answer)
    rl.close();
});

const fs = require('fs');

function versionNumberRead(version){

    let header = "/* FUWU.js vX.Y.Z - https://gitlab.com/DaGammla/fuwu.js - Released under the MIT license - https://gitlab.com/DaGammla/fuwu.js/-/blob/main/LICENSE - JJJJ by DaGammla */\n"

    let year = new Date().getFullYear();

    header = header.replace("X.Y.Z", version).replace("JJJJ", year);

    fs.readFile('fuwu.js', 'utf8', function(err, fuwuFile) {
        if (err) throw err;

        if (!fs.existsSync(version + "/"))
            fs.mkdirSync(version + "/")

        let fuwuMin = header + minify(fuwuFile);

        fs.writeFileSync(version + "/fuwu.min.js", fuwuMin);

        let sectionSplits = fuwuFile.split("//Section: ");

        for (let i = 1; i < sectionSplits.length; i++){
            let section = sectionSplits[i];
            let sectionName = "";

            [sectionName, section] = section.replace(/\s*\n/, "__§§§§§§§§__").split("__§§§§§§§§__");

            let sectionMin = header + minify(section);

            fs.writeFileSync(version + "/fuwu-" + sectionName + ".min.js", sectionMin);
        }

        fuwuFile = fuwuFile.replace("X.Y.Z", version).replace("JJJJ", year);
        fuwuFile = fuwuFile.replace(/\/\/Section: .+\s*\n/g, "");
        fs.writeFileSync(version + "/fuwu.js", fuwuFile);
    });

    fs.readFile('extensions.js', 'utf8', function(err, extFile) {
        if (err) throw err;

        if (!fs.existsSync(version + "/"))
            fs.mkdirSync(version + "/")

        let extMin = header + minify(extFile);

        fs.writeFileSync(version + "/extensions.min.js", extMin);

        extFile = extFile.replace("X.Y.Z", version).replace("JJJJ", year);
        fs.writeFileSync(version + "/extensions.js", extFile);
    });

    fs.readFile('fuwu-pl.js', 'utf8', function(err, plFile) {
        if (err) throw err;

        if (!fs.existsSync(version + "/"))
            fs.mkdirSync(version + "/")

        let plMin = header + minify(plFile);

        fs.writeFileSync(version + "/fuwu-pl.min.js", plMin);

        plFile = plFile.replace("X.Y.Z", version).replace("JJJJ", year);
        fs.writeFileSync(version + "/fuwu-pl.js", plFile);
    });
}